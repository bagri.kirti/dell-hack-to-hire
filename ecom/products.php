<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dell | India's Most trusted Brand</title>
    <link rel="stylesheet" href="./bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

</head>

<body>

    <?php
    include 'includes/header_menu.php';
    include 'includes/check-if-added.php';

    ?>


    <div class="banner">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-md-3"><br><br><br>
                <h1 class="banner-text">DELL</h1><br>
                <h3 class="banner-sub-text">Best Performance</h3>
            </div>
            <div class="col-md-8">
                <img class="foot-banner-img" src="images/banner.png">
            </div>
        </div>

    </div>

    <div class="container" style="margin-top:65px">

        <!-- <div class="jumbotron text-center">
            <h1>Welcome to Dell!</h1>
        </div> -->


        <!-- Button to Open the Modal -->
        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

        <!-- The Modal -->
        <?php
        $visible = false;
        if (isset($_POST['feedback'])) {
            require "includes/common.php";
            $email = $_SESSION['email'];
            $feedback = $_POST['feedback'];
            // echo($email);
            // echo($feedback);

            $quer = "INSERT INTO feedback(email,feedback) values('$email','$feedback')";
            if (!mysqli_query($con, $quer)) {
                echo (mysqli_error($con));
            }
        }


        ?>

        <div class="modal fade hide" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Hi!, Specially for you</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->

                    <div class="modal-body">
                        <div class="card" style="padding:1em;">
                            <img src="<?php echo ($img); ?>" style="height:193px;" alt="" class="img-fluid pb-1">
                            <div class="figure-caption">
                                <h5><?php echo ($name); ?></h5>
                                <h6><?php echo ($details); ?></h6>
                                <?php if (!isset($_SESSION['email'])) { ?>
                                    <p><a href="index.php#login" role="button" class="btn btn-warning  text-white ">Add To Cart</a></p>
                                <?php
                                } else {
                                    if (check_if_added_to_cart(2)) {
                                        echo '<p><a href="#" class="btn btn-warning  text-white" disabled>Added to cart</a></p>';
                                    } else {
                                        ?>
                                        <p><a href="cart-add.php?id=2" name="add" value="add" class="btn btn-warning  text-white">Add to cart</a></p>
                                    <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a style="color:white;" href="products.php?modal2=true" class="btn btn-danger">Not Interested</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade hide" id="modal2">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Your opinion matters to us</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->

                    <div class="modal-body">
                        <form action="./products.php" method="POST">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Feedback</span>
                                </div>
                                <textarea class="form-control" name="feedback" aria-label="With textarea"></textarea>
                                <button style="color:white" type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        </form>
                    </div>

                    <!-- Modal footer -->

                </div>
            </div>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>
        <hr />
        <form action="./products.php" method="GET">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputState">Ram</label>
                    <select name="ram" id="inputState" class="form-control">
                        <option selected>Choose...</option>
                        <option value="2">8gb</option>
                        <option value="3">16gb</option>
                        <option value="4">32gb</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label for="inputState">Graphics</label>
                    <select name="graphics" id="inputState" class="form-control">
                        <option selected>Choose...</option>
                        <option value="1">Entry</option>
                        <option value="2">Medium</option>
                        <option value="3">High</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputState">Processor</label>
                    <select name="processor" id="inputState" class="form-control">
                        <option selected>Choose...</option>
                        <option value="2">i5</option>
                        <option value="3">i7</option>
                        <option value="4">i9</option>
                    </select>
                </div>
            </div>
            <button type="submit" style="margin-left:40%;" class="btn btn-primary">GO!</button>
        </form>

        <div class="row text-center" id="shirt">
            <?php




            require "includes/common.php";


            $price = '(ram = 4 or ram=8 or ram=16 or ram=32)';
            $processor = "(processor like 'i3%' or processor like 'i5%' or processor like 'i7%' or processor like 'i9%')";
            $ram = '(ram = 4 or ram=8 or ram=16 or ram=32)';
            $graphics = '(graphics =1 or graphics=2 or graphics=3 or graphics=4)';

            if (isset($_GET['ram'])) {
                $r = $_GET['ram'];
                if ($r == 1) {
                    $r = 4;
                } else if ($r == 2) {
                    $r = 8;
                } else if ($r == 3) {
                    $r = 16;
                } else if ($r == 4) {
                    $r = 32;
                }
                if ($r == 4 || $r == 8 || $r == 32 || $r == 16)
                    $ram = 'ram = ' . $r . '';
                else {
                    $ram = '(ram = 4 or ram=8 or ram=16 or ram=32)';
                }
            }
            if (isset($_GET['graphics'])) {
                $r = $_GET['graphics'];
                if($r==1 || $r==2 || $r==4 || $r==4)
                $graphics = 'graphics = ' . $r . '';
                else{
                    $graphics = '(graphics =1 or graphics=2 or graphics=3 or graphics=4)';
                }
            }

            if (isset($_GET['processor'])) {
                $r = $_GET['processor'];
                if ($r == 1)
                    $processor = "processor like 'i3%'";
                else if ($r == 2)
                    $processor = "processor like 'i5%'";
                else if ($r == 3)
                    $processor = "processor like 'i7%'";
                else if ($r == 4)
                    $processor = "processor like 'i9%'";
                else 
                $processor = "(processor like 'i3%' or processor like 'i5%' or processor like 'i7%' or processor like 'i9%')";
            
            }
            $query = "SELECT * from products where " . $ram . ' and ' . $graphics . ' and ' . $processor;
            // echo($query);
            if (!mysqli_query($con, $query)) {
                echo (mysqli_error($con));
            }
            $result = mysqli_query($con, $query);
            while ($arrayResult = mysqli_fetch_array($result)) {
                $id = $arrayResult['id'];
                $name = $arrayResult['name'];
                $ram = $arrayResult['ram'];
                $price = $arrayResult['price'];
                $processor = $arrayResult['processor'];
                $img =  $arrayResult['image'];
                echo ('<div class="col-md-3 col-6 py-3">                 <div class="card">                     <img src="' . $img . '" alt="" class="img-fluid pb-1">                     <div class="figure-caption">                         <h5>' . $name . '</h5>                         <h6>' . $ram . 'gb ' . $processor . '</h6>  <h6>' . $price . '</h6>                                 <p><a href="cart-add.php?id=' . $id . '" name="add" value="add" class="btn btn-warning  text-white">Add to cart</a></p>                     </div>                 </div>             </div>');
            }



            ?>

        </div>





    </div>
    <?php include 'includes/footer.php' ?>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });
</script>
<?php if (isset($_GET['error'])) {
    $z = $_GET['error'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#signup').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";
} ?>
<?php if (isset($_GET['errorl'])) {
    $z = $_GET['errorl'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";
} ?>

        

<?php include 'chat.php'; ?>

</html>