<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    /* body{
    height:400px;
    position: fixed;
    bottom: 0;
} */


.chat-window{
    z-index: 100;
    bottom:-20px;;
    position:fixed;
    float:right;
    right:10px;
}
.chat-window > div > .panel{
    border-radius: 5px 5px 0 0;
}
.icon_minim{
    /* padding:2px 10px; */
}
.msg_container_base{
  background: #e5e5e5;
  margin: 0;
  padding: 0 10px 10px;
  max-height:400px;
  overflow-x:hidden;
}
.top-bar {
  background: #666;
  color: white;
  padding: 2px;
  position: relative;
  overflow: hidden;
}
.msg_receive{
    padding-left:0;
    margin-left:0;
}
.msg_sent{
    padding-bottom:20px !important;
    margin-right:0;
}
.messages {
  background: white;
  padding: 10px;
  border-radius: 2px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  max-width:100%;
}
.messages > p {
    font-size: 13px;
    margin: 0 0 0.2rem 0;
  }
.messages > time {
    font-size: 11px;
    color: #ccc;
}
.msg_container {
    padding: 10px;
    overflow: hidden;
    display: flex;
}
.img {
    display: block;
    width: 100%;
}
.avatar {
    position: relative;
}
.base_receive > .avatar:after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    width: 0;
    height: 0;
    border: 5px solid #FFF;
    border-left-color: rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0);
}

.base_sent {
  justify-content: flex-end;
  align-items: flex-end;
}
.base_sent > .avatar:after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 0;
    height: 0;
    border: 5px solid white;
    border-right-color: transparent;
    border-top-color: transparent;
    box-shadow: 1px 1px 2px rgba(black, 0.2); // not quite perfect but close
}

.msg_sent > time{
    float: right;
}



.msg_container_base::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

.btn-group.dropup{
    position:fixed;
    left:0px;
    bottom:0;
}

.panel-heading{
    height: 40px;
}

.panel {
  margin-bottom: 20px;
  background-color: #ffffff;
  border: 1px solid transparent;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
          box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}

.panel-body {
  padding: 15px;
}

.panel-body:before,
.panel-body:after {
  display: table;
  content: " ";
}

.panel-body:after {
  clear: both;
}

.panel-body:before,
.panel-body:after {
  display: table;
  content: " ";
}

.panel-body:after {
  clear: both;
}

.panel > .list-group {
  margin-bottom: 0;
}

.panel > .list-group .list-group-item {
  border-width: 1px 0;
}

.panel > .list-group .list-group-item:first-child {
  border-top-right-radius: 0;
  border-top-left-radius: 0;
}

.panel > .list-group .list-group-item:last-child {
  border-bottom: 0;
}

.panel-heading + .list-group .list-group-item:first-child {
  border-top-width: 0;
}

.panel > .table,
.panel > .table-responsive {
  margin-bottom: 0;
}

.panel > .panel-body + .table,
.panel > .panel-body + .table-responsive {
  border-top: 1px solid #dddddd;
}

.panel > .table-bordered,
.panel > .table-responsive > .table-bordered {
  border: 0;
}

.panel > .table-bordered > thead > tr > th:first-child,
.panel > .table-responsive > .table-bordered > thead > tr > th:first-child,
.panel > .table-bordered > tbody > tr > th:first-child,
.panel > .table-responsive > .table-bordered > tbody > tr > th:first-child,
.panel > .table-bordered > tfoot > tr > th:first-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > th:first-child,
.panel > .table-bordered > thead > tr > td:first-child,
.panel > .table-responsive > .table-bordered > thead > tr > td:first-child,
.panel > .table-bordered > tbody > tr > td:first-child,
.panel > .table-responsive > .table-bordered > tbody > tr > td:first-child,
.panel > .table-bordered > tfoot > tr > td:first-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > td:first-child {
  border-left: 0;
}

.panel > .table-bordered > thead > tr > th:last-child,
.panel > .table-responsive > .table-bordered > thead > tr > th:last-child,
.panel > .table-bordered > tbody > tr > th:last-child,
.panel > .table-responsive > .table-bordered > tbody > tr > th:last-child,
.panel > .table-bordered > tfoot > tr > th:last-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > th:last-child,
.panel > .table-bordered > thead > tr > td:last-child,
.panel > .table-responsive > .table-bordered > thead > tr > td:last-child,
.panel > .table-bordered > tbody > tr > td:last-child,
.panel > .table-responsive > .table-bordered > tbody > tr > td:last-child,
.panel > .table-bordered > tfoot > tr > td:last-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > td:last-child {
  border-right: 0;
}

.panel > .table-bordered > thead > tr:last-child > th,
.panel > .table-responsive > .table-bordered > thead > tr:last-child > th,
.panel > .table-bordered > tbody > tr:last-child > th,
.panel > .table-responsive > .table-bordered > tbody > tr:last-child > th,
.panel > .table-bordered > tfoot > tr:last-child > th,
.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > th,
.panel > .table-bordered > thead > tr:last-child > td,
.panel > .table-responsive > .table-bordered > thead > tr:last-child > td,
.panel > .table-bordered > tbody > tr:last-child > td,
.panel > .table-responsive > .table-bordered > tbody > tr:last-child > td,
.panel > .table-bordered > tfoot > tr:last-child > td,
.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > td {
  border-bottom: 0;
}

.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}

.panel-heading > .dropdown .dropdown-toggle {
  color: inherit;
}

.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
}

.panel-title > a {
  color: inherit;
}

.panel-footer {
  padding: 10px 15px;
  background-color: #f5f5f5;
  border-top: 1px solid #dddddd;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}

.panel-group .panel {
  margin-bottom: 0;
  overflow: hidden;
  border-radius: 4px;
}

.panel-group .panel + .panel {
  margin-top: 5px;
}

.panel-group .panel-heading {
  border-bottom: 0;
}

.panel-group .panel-heading + .panel-collapse .panel-body {
  border-top: 1px solid #dddddd;
}

.panel-group .panel-footer {
  border-top: 0;
}

.panel-group .panel-footer + .panel-collapse .panel-body {
  border-bottom: 1px solid #dddddd;
}

.panel-default {
  border-color: #dddddd;
}

.panel-default > .panel-heading {
  color: #333333;
  background-color: #f5f5f5;
  border-color: #dddddd;
}

.panel-default > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #dddddd;
}

.panel-default > .panel-heading > .dropdown .caret {
  border-color: #333333 transparent;
}

.panel-default > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #dddddd;
}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
.panel-primary {
  border-color: #428bca;
}

.panel-primary > .panel-heading {
  color: #ffffff;
  background-color: #428bca;
  border-color: #428bca;
}

.panel-primary > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #428bca;
}

.panel-primary > .panel-heading > .dropdown .caret {
  border-color: #ffffff transparent;
}

.panel-primary > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #428bca;
}

.panel-success {
  border-color: #d6e9c6;
}

.panel-success > .panel-heading {
  color: #468847;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}

.panel-success > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #d6e9c6;
}

.panel-success > .panel-heading > .dropdown .caret {
  border-color: #468847 transparent;
}

.panel-success > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #d6e9c6;
}

.panel-warning {
  border-color: #faebcc;
}

.panel-warning > .panel-heading {
  color: #c09853;
  background-color: #fcf8e3;
  border-color: #faebcc;
}

.panel-warning > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #faebcc;
}

.panel-warning > .panel-heading > .dropdown .caret {
  border-color: #c09853 transparent;
}

.panel-warning > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #faebcc;
}

.panel-danger {
  border-color: #ebccd1;
}

.panel-danger > .panel-heading {
  color: #b94a48;
  background-color: #f2dede;
  border-color: #ebccd1;
}

.panel-danger > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #ebccd1;
}

.panel-danger > .panel-heading > .dropdown .caret {
  border-color: #b94a48 transparent;
}

.panel-danger > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #ebccd1;
}

.panel-info {
  border-color: #bce8f1;
}

.panel-info > .panel-heading {
  color: #3a87ad;
  background-color: #d9edf7;
  border-color: #bce8f1;
}

.panel-info > .panel-heading + .panel-collapse .panel-body {
  border-top-color: #bce8f1;
}

.panel-info > .panel-heading > .dropdown .caret {
  border-color: #3a87ad transparent;
}

.panel-info > .panel-footer + .panel-collapse .panel-body {
  border-bottom-color: #bce8f1;
}

.chip {
    cursor:pointer;
    width: 150px;
    margin-top:10px;
  display: inline-block;
  padding: 0 25px;
  height: 25px;
  padding-top: 2px;
  padding-right:10px;
  font-size: 16px;
  /* line-height: 50px; */
  border-radius: 25px;
  background-color: #009dbd;
  color:white;
}

.chip .icon {
  float: left;
  margin: 2px 10px 20px -15px;
  height: 20px;
  width: 20px;
  border-radius: 50%;
}

    </style>
<!------ Include the above in your HEAD tag ---------->

<div class="container" >
    <div class="row chat-window col-xs-6 col-md-4" id="chat_window_1" style="margin-left:10px;">
        <div class="col-xs-12 col-md-12">
        	<div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="col-md-10 col-xs-10">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Dell <i onclick="clchat()" class="fa fa-window-close" aria-hidden="true"></i>
                    </h3>
                        
                    </div>
                    
                </div>
                <div class="panel-body msg_container_base" id="panel">
                    <div class="row msg_container base_sent">
                        <div class="col-md-10 col-xs-10">
                            <div class="messages msg_sent">
                                <p>Hey!, what purpose are you looking laptop for?</p>
                                <time datetime="2009-11-13T20:00">Timothy • 51 min</time>
                            </div>
                            <div onclick="gaming()" class="chip">
                                <i class="icon fa fa-gamepad "></i>Gaming
                            </div>
                            <br>
                            <div onclick="entertainment()" class="chip">
                                <i class="icon fa fa-ticket"></i>Entertainment
                            </div>
                            <br>
                            <div onclick="work()" class="chip">
                                <i class="icon fa fa-briefcase  "></i> Work
                            </div> 
                            <br>
                            <div onclick="design()" class="chip">
                                <i class="icon fa fa-video-camera"></i>Design
                            </div> 
                        </div>
                        <div class="col-md-2 col-xs-2 avatar">
                            <img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive ">
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                    </div>
                </div>
    		</div>
        </div>
    </div>
    
    
</div>

<script>
        
        function gaming(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>Which type of game would you like to play?</p></div><div onclick="farcry()" class="chip"><i class="icon fa fa-gamepad "></i>Far Cry 5</div><br><div onclick="gta()" class="chip"><i class="icon fa fa-gamepad "></i>Gta 5</div><br><div onclick="vicecity()" class="chip"><i class="icon fa fa-gamepad "></i> Vice city</div>  </div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function entertainment(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>Are you a music person or a movie person?</p></div><div onclick="music()" class="chip"><i class="icon fa fa-ticket "></i>Music</div><br><div onclick="movie()" class="chip"><i class="icon fa fa-ticket "></i>Movie</div><br><div onclick="both()" class="chip"><i class="icon fa fa-ticket "></i> Both</div>  </div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function work(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>What do you do?</p></div><div onclick="Finance()" class="chip"><i class="icon fa fa-briefcase "></i>Finance</div><br><div onclick="dev()" class="chip"><i class="icon fa fa-code "></i>Developer</div><br><div onclick="buisness()" class="chip"><i class="icon fa fa-briefcase "></i> Buisness</div>  </div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function design(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>What type of designer are you?</p></div><div onclick="gdesign()" class="chip"><i class="icon fa fa-camera "></i>Graphic</div><br><div onclick="videoeditor()" class="chip"><i class="icon fa fa-video-camera "></i>Video</div><br><div onclick="gamedev()" class="chip"><i class="icon fa fa-gamepad "></i> Game</div>  </div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
// dev
// fin
// buis

        function farcry(){

            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=1">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function gta(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=2">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function vicecity(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=3">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function music(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=4">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function movie(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=5">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function both(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=6">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function gdesign(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=7">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function videoeditor(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=8">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function gamedev(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=9">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function dev(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=9">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function Finance(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=6">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
        }
        function buisness(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var elem = document.createElement('div');
            elem.innerHTML = '<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>I would recommend you to go for this laptop<br><a style="color:blue; " href="details.php?id=4">this</a></p></div></div></div>';
            // console.log(para.text);
            var node = document.getElementById("panel");
            node.appendChild(elem);
            node.scrollTo(0, 500);
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            //
            // 
            // 
            // 
            // 
        }
        function clchat(){
            console.log("game");
            // var para = document.createElement('<div class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img class="img" src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img class="img"-responsive "></div><div class="col-xs-10 col-md-10"><div class="messages msg_receive"><p>that mongodb thing looks good, huh? tiny master db, and huge document store</p></div></div></div>')
            var node = document.getElementById("chat_window_1");
            node.style.visibility="hidden";
        }
</script>

<script>
    $(document).on('click', '.panel-heading span.icon_minim', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
    var $this = $(this);
    if ($('#minim_chat_window').hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideDown();
        $('#minim_chat_window').removeClass('panel-collapsed');
        $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '#new_chat', function (e) {
    var size = $( ".chat-window:last-child" ).css("margin-left");
     size_total = parseInt(size) + 400;
    alert(size_total);
    var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
    clone.css("margin-left", size_total);
});
$(document).on('click', '.icon_close', function (e) {
    //$(this).parent().parent().parent().parent().remove();
    $( "#chat_window_1" ).remove();
});

    </script>

  