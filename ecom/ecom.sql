-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 08, 2019 at 11:26 AM
-- Server version: 5.7.27-0ubuntu0.19.04.1
-- PHP Version: 7.2.19-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `email` varchar(100) NOT NULL,
  `feedback` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`email`, `feedback`) VALUES
('viduuj@gmal.com', 'Hello'),
('viduuj@gmal.com', 'Because bakwaas hai'),
('viduuj@gmal.com', 'nnnnnnn'),
('viduuj@gmal.com', 'dena\r\n'),
('viduuj@gmal.com', 'dena\r\n'),
('bagri.kirti296@gmail.com', 'lfgskvysiyizgtsuvgyuayiay'),
('', 'okay'),
('', ''),
('', 'Feebback'),
('viduuj@gmal.com', 'asd'),
('viduuj@gmal.com', 'asd'),
('viduuj@gmal.com', 'asdasd'),
('viduuj@gmal.com', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ram` varchar(100) NOT NULL,
  `processor` varchar(100) NOT NULL,
  `price` varchar(100) NOT NULL,
  `graphics` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `family_id` varchar(100) NOT NULL,
  `display` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `ram`, `processor`, `price`, `graphics`, `image`, `family_id`, `display`) VALUES
(1, 'AlienWare M17', '32', 'i9 8th Gen', '180000', '4', 'images/alien2.png', 'gaming_laptops', 'Full HD'),
(2, 'Alienware M15', '32', 'i7 8th Gen', '120000', '4', 'images/alien1.jpg', 'laptop', ''),
(3, 'G7 7588', '16', 'i7 8th Gen', '70000', '3', 'images/g7.jpg', '', ''),
(4, 'Insperion 5567', '8', 'i5 8th Gen', '52000', '2', 'images/5567.jpg', '', ''),
(5, 'Insperion 5579', '8', 'i5 8th Gen', '56000', '3', 'images/5579.jpg', '', ''),
(6, 'G5 5432', '8', 'i7 8th Gen', '59000', '3', 'images/g5.jpg', '', ''),
(7, 'Latitude 2900', '16', 'i7 8th Gen', '69000', '3', 'images/touch.jpg', '', ''),
(8, 'Latitude 1125', '16', 'i7 8th Gen', '58000', '1', 'images/lap3.jpeg', '', ''),
(9, 'XPS', '16', 'i9 8th Gen', '90,000', '3', 'images/xps.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `phone` int(10) NOT NULL,
  `registration_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email_id`, `first_name`, `last_name`, `phone`, `registration_time`, `password`) VALUES
(65, 'sharew5m123@gmail.com', 'reys', 'rudt', 0, '2019-03-18 13:46:33', 'e4f194cba29960e12d8b8f1bfedc972b'),
(66, 'sgah234@gmail.com', 'werty', 'erty', 0, '2019-03-18 13:55:46', 'e10adc3949ba59abbe56e057f20f883e'),
(67, 'sham1234@gmail.com', 'Sham', 'das', 0, '2019-03-19 07:37:46', 'e10adc3949ba59abbe56e057f20f883e'),
(68, 'mail@mail.com', 'mail', 'man', 1869530027, '2019-08-06 05:13:05', 'strawhat'),
(69, 'viduuj@gmal.com', 'vidu', 'jain', 0, '2019-08-06 05:35:02', 'strawhat'),
(70, 'bagri.kirti296@gmail.com', 'Kirti', 'Bagri', 0, '2019-08-06 11:24:01', 'qwerty123');

-- --------------------------------------------------------

--
-- Table structure for table `users_products`
--

CREATE TABLE `users_products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `status` enum('Added To Cart','Confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_products`
--
ALTER TABLE `users_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `users_products`
--
ALTER TABLE `users_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_products`
--
ALTER TABLE `users_products`
  ADD CONSTRAINT `users_products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_products_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `products` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
