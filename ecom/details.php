<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dell  | India's Most trusted Brand</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
    <link href='https://fonts.googleapis.com/css?family=Delius Swash Caps' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Andika' rel='stylesheet'>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

</head>
<body>

 <?php
include 'includes/header_menu.php';
include 'includes/check-if-added.php';
?>



<div class="container" style="margin-top:65px">
        
        <!-- <div class="jumbotron text-center">
            <h1>Welcome to Dell!</h1>
        </div> -->
                

        <!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->
<?php
            if (isset($_GET['modal2'])) {
                session_start();
                echo ('<script type="text/javascript">');
    
                echo ("$(window).on('load',function(){");
                echo ("$('#modal2').modal('show');");
                echo ("});</script> ");
            }
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $name = "";
                $details = "";
                $price = "";
                $img = "";
                if($id==1){
                    $name="Alienware M17";
                    $details="32gb i9 8th Gen, DDR4";
                    $price="1,80000";
                    $img="images/alien2.png";
                }

                if($id==2){
                    $name="Alienware M15";
                    $details="32gb i7 8th Gen, DDR4";
                    $price="1,20000";
                    $img="images/alien1.jpg";
                }
                if($id==3){
                    $name="G7 7588";
                    $details="16gb i7 8th Gen, DDR4";
                    $price="70,000";
                    $img="images/g7.jpg";
                }
                if($id==4){
                    $name="Insperion 5567";
                    $details="8gb i5 8th Gen, DDR4";
                    $price="52,000";
                    $img="images/5567.jpg";
                }
                if($id==5){
                    $name="Insperion 5579";
                    $details="8gb i5 8th Gen, DDR4";
                    $price="56,000";
                    $img="images/5579.jpg";
                }
                if($id==6){
                    $name="G5 5432";
                    $details="8gb i7 8th Gen, DDR4";
                    $price="59,000";
                    $img="images/g5.jpg";
                }
                if($id==7){
                    $name="Latitude 2900";
                    $details="16gb i7 8th Gen, DDR4";
                    $price="69,000";
                    $img="images/touch.jpg";
                }
                if($id==8){
                    $name="Latitude 1125";
                    $details="16gb i7 8th Gen, DDR4";
                    $price="58,000";
                    $img="images/lap3.jpeg";
                }
                if($id==9){
                    $name="XPS";
                    $details="16gb i9 8th Gen, DDR4";
                    $price="90,0000";
                    $img="images/xps.jpg";
                }
                
            }       
      ?>


<div class="modal fade hide" id="modal2">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Your opinion matters to us</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->

                    <div class="modal-body">
                        <form action="./products.php" method="POST">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Feedback</span>
                                </div>
                                <textarea class="form-control" name="feedback" aria-label="With textarea"></textarea>
                                <button style="color:white" type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        </form>
                    </div>

                    <!-- Modal footer -->

                </div>
            </div>
        </div>






<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo($name)?></li>
            </ol>
        </nav>
    <hr/>
    <div class="row text-center" id="watch">
        <div class="col-md-3 col-6 py-2">
            <div class="card">
                <img src="<?php echo($img)?>" class="img-fluid pb-1" >
                <div class="figure-caption">
                    <h5><?php echo($name) ?></h5>
                    <h6><?php echo($details) ?></h6>
                        <p><a href="cart-add.php?id=<?php echo($id)?>" name="add" value="add" class="btn btn-warning  text-white">Add to cart</a> <a href="details.php?modal2=true" name="add" value="add" class="btn btn-danger  text-white">Not Interested</a><p>
                </div>
            </div>
        </div>
      </div>
</div>
        <?php include 'includes/footer.php'?>
      
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
</script>
<?php if (isset($_GET['error'])) {$z = $_GET['error'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#signup').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";}?>
<?php if (isset($_GET['errorl'])) {$z = $_GET['errorl'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";}?>



</html>


