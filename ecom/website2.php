<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laptops</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
    <link href='https://fonts.googleapis.com/css?family=Delius Swash Caps' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Andika' rel='stylesheet'>
    <link rel="stylesheet" href="style2.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

</head>
<body>
<nav class="shadow-lg  navbar fixed-top navbar-expand-sm navbar-dark" style="background-color:#3e2723">
            <div class="container">
                    <a href="index.php" class="navbar-brand" style="">LaptopKart</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mynavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="mynavbar">
                    <ul class="nav navbar-nav">
                       
                    </ul>
                    </div>
                </div>
            </div>
        </nav>

 <?php
include 'includes/check-if-added.php';
session_start();
    
if(isset($_POST['laptop'])){
    $laptop = $_POST['laptop'];
    $laptop = strtolower($laptop);
    $laptop = str_replace(" ","-",$laptop);
    $_SESSION["laptop"] = $laptop;
}
if(isset($_POST['ram'])){
    $ram = $_POST['ram'];
    $_SESSION["ram"] = $ram;
}
if(isset($_POST['graphics'])){
    $graphic = $_POST['graphics'];
    $_SESSION["graphics"] = $graphic;
}
if(isset($_POST['processor'])){
    $processor = $_POST['processor'];
    $_SESSION["processor"] = $processor;
}
?>


<div class="banner">
    <div class="row">
        <div class ="col-sm-1"></div>
    <div class="col-md-3"><br><br><br><h1 class="banner-text">LaptopKart</h1><br></div> 
    <div class="col-md-8">
        <img class="foot-banner-img" src="images/web2ban.png"> 
    </div>
    </div>

</div>

<div class="container" style="margin-top:65px">
        
        <!-- <div class="jumbotron text-center">
            <h1>Welcome to Dell!</h1>
        </div> -->
                

        <!-- Button to Open the Modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button> -->

<!-- The Modal -->

<!-- <form action="./website2.php" method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Search Laptops</label>
    <input  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="laptop" placeholder="Enter Search Query">
  </div>
  <button type="submit" class="btn btn-warning">Submit</button>
</form> -->

<form  action="./website2.php" method="POST">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Laptop name</label>
      <input  class="form-control" name="laptop" id="inputEmail4" placeholder="Search">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputState">Ram</label>
      <select id="inputState" class="form-control">
        <option name="ram" selected>Choose...</option>
        <option value="1">4 gb</option>
        <option value="2">8 gb</option>
        <option value="3">16 gb</option>
        <option value="4">32 gb</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">Graphics</label>
      <select name="graphics" id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option value="1">GTx Mx150</option>
        <option value="2">Gtx 1080</option>
        <option value="3">Titan Ti</option>
      </select>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputState">Processor</label>
      <select id="inputState" class="form-control">
        <option name="processor" selected>Choose...</option>
        <option value="1">i3</option>
        <option value="2">i5</option>
        <option value="3">i7</option>
        <option value="4">i9</option>
      </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Sign in</button>
</form>
<br>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>
    <hr/>
    
      </div>
        <?php include 'includes/footer.php'?>
      
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
</script>
<?php if (isset($_GET['error'])) {$z = $_GET['error'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#signup').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";}?>
<?php if (isset($_GET['errorl'])) {$z = $_GET['errorl'];
    echo "<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>";
    echo "<script type='text/javascript'>alert('" . $z . "')</script>";}?>
</html>
